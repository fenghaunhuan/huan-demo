package com.huan.test.shardingjdbc.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.huan.test.shardingjdbc.entity.Order;
import com.huan.test.shardingjdbc.repository.OrderRepository;

/**
 * Order 服务对象.
 * 
 * @author gaohongtao
 */
@Service
@Transactional
public class OrderService {
    
    @Resource
    private OrderRepository orderRepository;
    
    @Transactional(readOnly = true)
    public void select() {
        List<Order> list = orderRepository.selectAll();
        System.out.println(list);;
        System.out.println(list.size());
    }
    
    public void clear() {
        orderRepository.deleteAll();
    }
    
    public void fooService() {
        Order criteria = new Order();
        criteria.setUserId(10);
        criteria.setOrderId(1);
        criteria.setStatus("INSERT");
        orderRepository.insert(criteria);
        criteria.setUserId(11);
        criteria.setOrderId(1);
        criteria.setStatus("INSERT2");
        orderRepository.insert(criteria);
        orderRepository.update(Lists.newArrayList(10, 11));
    }
    
    public void fooServiceWithFailure() {
        fooService();
        throw new IllegalArgumentException("failed");
    }
}
