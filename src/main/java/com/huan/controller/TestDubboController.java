package com.huan.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.credit.core.controller.BaseController;
import com.credit.core.service.dubbo.BaseDubboService;
import com.credit.core.service.dubbo.redis.RedisDubboService;

@RestController
public class TestDubboController extends BaseController{
    
    private Logger log = Logger.getLogger(UserController.class);

    @Autowired
    private BaseDubboService baseDubboService;
    @Autowired
    private RedisDubboService redisDubboService;
    
    @RequestMapping(value="/test" , method=RequestMethod.GET,produces={"application/xml", "application/json"})
    @ResponseBody
    public void user(HttpServletRequest request,HttpServletResponse response,
            ModelMap model){
        String result = baseDubboService.test("Hello World!");
        log.info("rpc服务端返回："+result);
        System.out.println(redisDubboService.set("test:1", "test"));
        System.out.println(redisDubboService.get("test:1"));
        model.put("result", result);
    }
    
}
