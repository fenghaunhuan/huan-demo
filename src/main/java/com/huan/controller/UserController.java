package com.huan.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.alibaba.fastjson.JSON;
import com.credit.core.controller.BaseController;
import com.credit.core.service.dubbo.redis.RedisDubboService;
import com.huan.service.UserService;

@RestController
public class UserController extends BaseController{
    
    private Logger log = Logger.getLogger(UserController.class);
    
    @Autowired
    private UserService userService;
    
    @Autowired
    private RedisDubboService redisDuboService;
    
    @RequestMapping(value="/user" , method=RequestMethod.GET,produces={"application/xml", "application/json"})
    @ResponseBody
    public String user(HttpServletRequest request,HttpServletResponse response,
            ModelMap model){
        Map<String , Object> entity = new HashMap<>();
        entity.put("id", redisDuboService.getPrimaryId("user", System.currentTimeMillis()));
        entity.put("name", "test"+System.currentTimeMillis());
        entity.put("sex", (0+Math.random()*(1-0+1)));
        entity.put("age", (0+Math.random()*(100-0+1)));
        userService.insertByAn(entity);
        model.put("success", true);
        return JSON.toJSONString(model);
    }
    
    @RequestMapping(value="/getUser" , method=RequestMethod.GET,produces={"application/xml", "application/json"})
    @ResponseBody
    public String getUser(HttpServletRequest request,HttpServletResponse response,
            ModelMap model){
       try {
               model.put("success", true);
               model.put("list", userService.selectAll());
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e.getMessage());
            }
       return JSON.toJSONString(model);
    }
    
}
