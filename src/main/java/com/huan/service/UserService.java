package com.huan.service;

import java.util.List;
import java.util.Map;

import com.huan.model.User;

public interface UserService {
    
    int insert(User user);
    
    List<User> get();
    
    List<User> selectAll();
    
    void insertByAn(Map<String , Object> entity);  
      
    void deleteById(Map<String , Object> entity);  
      
    void update(Map<String , Object> entity);  
    
    User selectById(Map<String , Object> entity);
}
