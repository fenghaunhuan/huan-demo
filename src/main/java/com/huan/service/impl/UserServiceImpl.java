package com.huan.service.impl;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.huan.dao.UserDao;
import com.huan.model.User;
import com.huan.service.UserService;

import tk.mybatis.mapper.common.Mapper;

@Service
public class UserServiceImpl implements UserService {
    
    @Resource
    private Mapper<User> userMapper;
    
    @Autowired
    private UserDao userDao;

    @Override
    public int insert(User user) {
        return userMapper.insert(user);
    }

    @Override
    public List<User> get() {
        return userMapper.selectAll();
    }

    @Override
    public List<User> selectAll() {
        return userDao.selectAll();
    }

    @Override
    public void insertByAn(Map<String , Object> entity) {
         userDao.insert(entity);
    }

    @Override
    public void deleteById(Map<String , Object> entity) {
        userDao.deleteById(entity);
    }

    @Override
    public void update(Map<String , Object> entity) {
        userDao.update(entity);
    }

    @Override
    public User selectById(Map<String , Object> entity) {
        return userDao.selectById(entity);
    }

}
