package com.huan.model;


import java.io.Serializable;

import javax.persistence.*;

@Table(name = "user")
public class User implements Serializable{
    
    private static final long serialVersionUID = 8848268070382394755L;

    @Id
    @Column(name = "id")
  /*  @GeneratedValue(strategy = GenerationType.IDENTITY)*/
    private Long id;
    
    @Column(name="name")
    private String name;
    
    @Column(name="sex")
    private Integer sex;
    
    @Column(name="age")
    private Integer age;

    /**
     * @return id
     */
    public Long getId() {
        return id;
    }

    /**
     * @param id
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return sex
     */
    public Integer getSex() {
        return sex;
    }

    /**
     * @param sex
     */
    public void setSex(Integer sex) {
        this.sex = sex;
    }

    /**
     * @return age
     */
    public Integer getAge() {
        return age;
    }

    /**
     * @param age
     */
    public void setAge(Integer age) {
        this.age = age;
    }
}