package com.huan.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.huan.model.User;

public interface UserDao  {
    
    @Results( value={
            @Result(property="id",column="id",id=true),
            @Result(property="name",column="name"),
            @Result(property="sex",column="sex"),
            @Result(property="age",column="age")
    })
    @Select("SELECT * FROM user")
    List<User>  selectAll();
    
    @Results( value={
            @Result(property="id",column="id",id=true),
            @Result(property="name",column="name"),
            @Result(property="sex",column="sex"),
            @Result(property="age",column="age")
    })
    @Select("SELECT * FROM user WHERE id = #{id}")
    User selectById(Map<String , Object> entity);

    @Insert("INSERT INTO user (id , name , age , sex ) VALUES(#{id} , #{name} ,#{age} ,#{sex})")
    int insert(Map<String , Object> entity);

    @Delete("delete from users where id = #{id}")  
     int deleteById(Map<String , Object> entity);  
      
    @Update("update user set name = #{name}, age = #{age} , sex = #{sex} where id = #{id}")  
     int update(Map<String , Object> entity);  
      
    
}
