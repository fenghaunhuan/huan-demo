/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : test2

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-03-22 16:16:58
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user_0
-- ----------------------------
DROP TABLE IF EXISTS `user_0`;
CREATE TABLE `user_0` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_0
-- ----------------------------
INSERT INTO `user_0` VALUES ('1977', 'test1458037928551', '0', '74');
INSERT INTO `user_0` VALUES ('7237', 'test1458037925613', '0', '85');
INSERT INTO `user_0` VALUES ('9677', 'test1458037928583', '0', '12');
INSERT INTO `user_0` VALUES ('11095', 'test1458037923303', '0', '95');
INSERT INTO `user_0` VALUES ('12417', 'test1458111957567', '0', '34');
INSERT INTO `user_0` VALUES ('12821', 'test1458553912554', '0', '98');
INSERT INTO `user_0` VALUES ('13361', 'test1458037925382', '0', '30');
INSERT INTO `user_0` VALUES ('15139', 'test1458037929740', '0', '20');
INSERT INTO `user_0` VALUES ('16303', 'test1458037922324', '0', '57');
INSERT INTO `user_0` VALUES ('23111', 'test1458037922467', '0', '34');
INSERT INTO `user_0` VALUES ('30539', 'test1458037925019', '0', '93');
INSERT INTO `user_0` VALUES ('31975', 'test1458037924722', '0', '83');
INSERT INTO `user_0` VALUES ('36805', 'test1458037924458', '0', '94');
INSERT INTO `user_0` VALUES ('40347', 'test1458037925283', '0', '17');
INSERT INTO `user_0` VALUES ('41457', 'test1458555250576', '1', '47');
INSERT INTO `user_0` VALUES ('45373', 'test1458037923205', '0', '50');
INSERT INTO `user_0` VALUES ('46753', 'test1458037924095', '0', '24');
INSERT INTO `user_0` VALUES ('47153', 'test1458037928683', '0', '36');
INSERT INTO `user_0` VALUES ('47643', 'test1458555263601', '1', '41');
INSERT INTO `user_0` VALUES ('47713', 'test1458037925449', '0', '93');
INSERT INTO `user_0` VALUES ('47805', 'test1458037930964', '0', '2');
INSERT INTO `user_0` VALUES ('51695', 'test1458553912378', '0', '23');
INSERT INTO `user_0` VALUES ('56155', 'test1458037928387', '0', '30');
INSERT INTO `user_0` VALUES ('57789', 'test1458629610367', '1', '12');
INSERT INTO `user_0` VALUES ('59745', 'test1458037929442', '0', '87');
INSERT INTO `user_0` VALUES ('63809', 'test1458037928320', '0', '43');
INSERT INTO `user_0` VALUES ('64725', 'test1458037931099', '0', '73');
INSERT INTO `user_0` VALUES ('67961', 'test1458037921859', '0', '16');
INSERT INTO `user_0` VALUES ('72515', 'test1458037925217', '0', '30');
INSERT INTO `user_0` VALUES ('74415', 'test1458037924821', '0', '59');
INSERT INTO `user_0` VALUES ('76287', 'test1458037924656', '0', '93');
INSERT INTO `user_0` VALUES ('77867', 'test1458037924921', '0', '81');
INSERT INTO `user_0` VALUES ('81061', 'test1458037928487', '0', '97');
INSERT INTO `user_0` VALUES ('84259', 'test1458037929014', '0', '33');
INSERT INTO `user_0` VALUES ('85373', 'test1458037929708', '0', '47');
INSERT INTO `user_0` VALUES ('87191', 'test1458037927821', '0', '48');
INSERT INTO `user_0` VALUES ('89813', 'test1458037923403', '0', '98');
INSERT INTO `user_0` VALUES ('93227', 'test1458037923897', '0', '35');
INSERT INTO `user_0` VALUES ('93987', 'test1458037924194', '0', '76');
INSERT INTO `user_0` VALUES ('95085', 'test1458111856674', '0', '99');

-- ----------------------------
-- Table structure for user_1
-- ----------------------------
DROP TABLE IF EXISTS `user_1`;
CREATE TABLE `user_1` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_1
-- ----------------------------
INSERT INTO `user_1` VALUES ('2817', 'test1458111656008', '0', '57');
INSERT INTO `user_1` VALUES ('3273', 'test1458037929542', '0', '38');
INSERT INTO `user_1` VALUES ('6457', 'test1458037928355', '0', '24');
INSERT INTO `user_1` VALUES ('14889', 'test1458037924261', '0', '67');
INSERT INTO `user_1` VALUES ('16035', 'test1458037928718', '0', '58');
INSERT INTO `user_1` VALUES ('16283', 'test1458553913818', '0', '29');
INSERT INTO `user_1` VALUES ('17183', 'test1458111856275', '0', '4');
INSERT INTO `user_1` VALUES ('20659', 'test1458037928849', '0', '87');
INSERT INTO `user_1` VALUES ('21177', 'test1458037928981', '0', '11');
INSERT INTO `user_1` VALUES ('21179', 'test1458037929476', '0', '86');
INSERT INTO `user_1` VALUES ('21291', 'test1458037924689', '0', '83');
INSERT INTO `user_1` VALUES ('24087', 'test1458037929080', '0', '91');
INSERT INTO `user_1` VALUES ('26641', 'test1458037925482', '0', '87');
INSERT INTO `user_1` VALUES ('29673', 'test1458037923533', '0', '88');
INSERT INTO `user_1` VALUES ('33815', 'test1458111676587', '0', '39');
INSERT INTO `user_1` VALUES ('35465', 'test1458630166603', '1', '29');
INSERT INTO `user_1` VALUES ('38249', 'test1458037923171', '0', '85');
INSERT INTO `user_1` VALUES ('41669', 'test1458037924161', '0', '63');
INSERT INTO `user_1` VALUES ('46181', 'test1458037929673', '0', '64');
INSERT INTO `user_1` VALUES ('48593', 'test1458037925516', '0', '93');
INSERT INTO `user_1` VALUES ('48789', 'test1458037922027', '0', '4');
INSERT INTO `user_1` VALUES ('54507', 'test1458037924953', '0', '93');
INSERT INTO `user_1` VALUES ('59233', 'test1458037929146', '0', '40');
INSERT INTO `user_1` VALUES ('61993', 'test1458037923601', '0', '20');
INSERT INTO `user_1` VALUES ('63327', 'test1458037924492', '0', '85');
INSERT INTO `user_1` VALUES ('78891', 'test1458037928618', '0', '97');
INSERT INTO `user_1` VALUES ('78941', 'test1458553913276', '0', '38');
INSERT INTO `user_1` VALUES ('80249', 'test1458037923963', '0', '84');
INSERT INTO `user_1` VALUES ('81199', 'test1458037931443', '0', '61');
INSERT INTO `user_1` VALUES ('82697', 'test1458553913074', '0', '8');
INSERT INTO `user_1` VALUES ('87415', 'test1458037928915', '0', '84');
INSERT INTO `user_1` VALUES ('88623', 'test1458037924986', '0', '13');
INSERT INTO `user_1` VALUES ('90951', 'test1458037925349', '0', '71');
INSERT INTO `user_1` VALUES ('94629', 'test1458037929310', '0', '27');
INSERT INTO `user_1` VALUES ('94669', 'test1458037925416', '0', '8');
INSERT INTO `user_1` VALUES ('96917', 'test1458037929047', '0', '93');
INSERT INTO `user_1` VALUES ('97573', 'test1458037923236', '0', '27');
