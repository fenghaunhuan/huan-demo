/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50709
Source Host           : localhost:3306
Source Database       : test1

Target Server Type    : MYSQL
Target Server Version : 50709
File Encoding         : 65001

Date: 2016-03-22 16:16:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for user_0
-- ----------------------------
DROP TABLE IF EXISTS `user_0`;
CREATE TABLE `user_0` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_0
-- ----------------------------
INSERT INTO `user_0` VALUES ('4080', 'test1458037925152', '0', '18');
INSERT INTO `user_0` VALUES ('4234', 'test1458037860125', '0', '29');
INSERT INTO `user_0` VALUES ('5520', 'test1458037929409', '0', '28');
INSERT INTO `user_0` VALUES ('7006', 'test1458037929113', '0', '89');
INSERT INTO `user_0` VALUES ('9514', 'test1458037925119', '0', '92');
INSERT INTO `user_0` VALUES ('10696', 'test1458037925052', '0', '60');
INSERT INTO `user_0` VALUES ('10730', 'test1458037925086', '0', '30');
INSERT INTO `user_0` VALUES ('14386', 'test1458037924360', '0', '51');
INSERT INTO `user_0` VALUES ('14482', 'test1458553912884', '0', '96');
INSERT INTO `user_0` VALUES ('15554', 'test1458629705837', '1', '95');
INSERT INTO `user_0` VALUES ('16140', 'test1458553912714', '0', '55');
INSERT INTO `user_0` VALUES ('16528', 'test1458037859171', '0', '37');
INSERT INTO `user_0` VALUES ('17498', 'test1458037924062', '0', '86');
INSERT INTO `user_0` VALUES ('18562', 'test1458037923502', '0', '47');
INSERT INTO `user_0` VALUES ('25712', 'test1458037928421', '0', '26');
INSERT INTO `user_0` VALUES ('26238', 'test1458037929776', '0', '45');
INSERT INTO `user_0` VALUES ('27714', 'test1458037656180', '0', '46');
INSERT INTO `user_0` VALUES ('29950', 'test1458037859671', '0', '75');
INSERT INTO `user_0` VALUES ('30946', 'test1458037928751', '0', '80');
INSERT INTO `user_0` VALUES ('31486', 'test1458037929376', '0', '61');
INSERT INTO `user_0` VALUES ('33634', 'test1458037923996', '0', '39');
INSERT INTO `user_0` VALUES ('34786', 'test1458037921181', '0', '75');
INSERT INTO `user_0` VALUES ('35424', 'test1458037924623', '0', '25');
INSERT INTO `user_0` VALUES ('37828', 'test1458037852411', '0', '38');
INSERT INTO `user_0` VALUES ('42886', 'test1458037928781', '0', '63');
INSERT INTO `user_0` VALUES ('43094', 'test1458037929509', '0', '13');
INSERT INTO `user_0` VALUES ('46072', 'test1458037929932', '0', '68');
INSERT INTO `user_0` VALUES ('49858', 'test1458037931251', '0', '64');
INSERT INTO `user_0` VALUES ('50522', 'test1458037923733', '0', '70');
INSERT INTO `user_0` VALUES ('51876', 'test1458037925579', '0', '53');
INSERT INTO `user_0` VALUES ('51882', 'test1458037924558', '0', '92');
INSERT INTO `user_0` VALUES ('52672', 'test1458037929640', '0', '54');
INSERT INTO `user_0` VALUES ('57328', 'test1458554241521', '1', '51');
INSERT INTO `user_0` VALUES ('57992', 'test1458037929277', '0', '83');
INSERT INTO `user_0` VALUES ('59084', 'test1458037930403', '0', '18');
INSERT INTO `user_0` VALUES ('62030', 'test1458037925250', '0', '23');
INSERT INTO `user_0` VALUES ('62658', 'test1458037702316', '0', '51');
INSERT INTO `user_0` VALUES ('64506', 'test1458037924028', '0', '89');
INSERT INTO `user_0` VALUES ('65500', 'test1458037859500', '0', '83');
INSERT INTO `user_0` VALUES ('65678', 'test1458037929806', '0', '89');
INSERT INTO `user_0` VALUES ('66070', 'test1458037859837', '0', '38');
INSERT INTO `user_0` VALUES ('68714', 'test1458553913651', '0', '54');
INSERT INTO `user_0` VALUES ('69654', 'test1458037929609', '0', '4');
INSERT INTO `user_0` VALUES ('71958', 'test1458037929343', '0', '38');
INSERT INTO `user_0` VALUES ('73104', 'test1458553913458', '0', '53');
INSERT INTO `user_0` VALUES ('75860', 'test1458037923567', '0', '15');
INSERT INTO `user_0` VALUES ('76218', 'test1458037924393', '0', '80');
INSERT INTO `user_0` VALUES ('81186', 'test1458037928453', '0', '5');
INSERT INTO `user_0` VALUES ('82388', 'test1458037924525', '0', '88');
INSERT INTO `user_0` VALUES ('83458', 'test1458630024193', '1', '96');
INSERT INTO `user_0` VALUES ('86842', 'test1458037851765', '0', '47');
INSERT INTO `user_0` VALUES ('87546', 'test1458037923436', '0', '42');
INSERT INTO `user_0` VALUES ('87676', 'test1458037924227', '0', '66');
INSERT INTO `user_0` VALUES ('89778', 'test1458037929212', '0', '21');
INSERT INTO `user_0` VALUES ('94638', 'test1458037923138', '0', '25');
INSERT INTO `user_0` VALUES ('96026', 'test1458037851437', '0', '66');
INSERT INTO `user_0` VALUES ('96780', 'test1458037925185', '0', '56');
INSERT INTO `user_0` VALUES ('97222', 'test1458037930540', '0', '40');

-- ----------------------------
-- Table structure for user_1
-- ----------------------------
DROP TABLE IF EXISTS `user_1`;
CREATE TABLE `user_1` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sex` int(11) DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_1
-- ----------------------------
INSERT INTO `user_1` VALUES ('912', 'test1458037924128', '0', '43');
INSERT INTO `user_1` VALUES ('2462', 'test1458553911819', '0', '3');
INSERT INTO `user_1` VALUES ('4966', 'test1458037923930', '0', '7');
INSERT INTO `user_1` VALUES ('5996', 'test1458037928882', '0', '45');
INSERT INTO `user_1` VALUES ('6348', 'test1458037923799', '0', '20');
INSERT INTO `user_1` VALUES ('6526', 'test1458037928518', '0', '50');
INSERT INTO `user_1` VALUES ('9008', 'test1458037924592', '0', '77');
INSERT INTO `user_1` VALUES ('11002', 'test1458553896877', '0', '6');
INSERT INTO `user_1` VALUES ('11166', 'test1458037923370', '0', '45');
INSERT INTO `user_1` VALUES ('12014', 'test1458037922179', '0', '12');
INSERT INTO `user_1` VALUES ('13180', 'test1458553912130', '0', '4');
INSERT INTO `user_1` VALUES ('19562', 'test1458037930251', '0', '1');
INSERT INTO `user_1` VALUES ('21956', 'test1458037925546', '0', '98');
INSERT INTO `user_1` VALUES ('22742', 'test1458037928815', '0', '5');
INSERT INTO `user_1` VALUES ('23588', 'test1458037923468', '0', '25');
INSERT INTO `user_1` VALUES ('25474', 'test1458037923337', '0', '58');
INSERT INTO `user_1` VALUES ('27304', 'test1458111855836', '0', '99');
INSERT INTO `user_1` VALUES ('29434', 'test1458037928948', '0', '11');
INSERT INTO `user_1` VALUES ('33256', 'test1458037858803', '0', '17');
INSERT INTO `user_1` VALUES ('35766', 'test1458037859981', '0', '37');
INSERT INTO `user_1` VALUES ('36998', 'test1458037929179', '0', '93');
INSERT INTO `user_1` VALUES ('38734', 'test1458037925316', '0', '42');
INSERT INTO `user_1` VALUES ('40940', 'test1458037921652', '0', '97');
INSERT INTO `user_1` VALUES ('41724', 'test1458037661114', '0', '4');
INSERT INTO `user_1` VALUES ('41882', 'test1458037924327', '0', '48');
INSERT INTO `user_1` VALUES ('45024', 'test1458037923668', '0', '34');
INSERT INTO `user_1` VALUES ('46712', 'test1458037922603', '0', '70');
INSERT INTO `user_1` VALUES ('50026', 'test1458037923699', '0', '28');
INSERT INTO `user_1` VALUES ('51594', 'test1458037924426', '0', '4');
INSERT INTO `user_1` VALUES ('59526', 'test1458037924294', '0', '63');
INSERT INTO `user_1` VALUES ('60678', 'test1458118908049', '0', '18');
INSERT INTO `user_1` VALUES ('61446', 'test1458037928650', '0', '97');
INSERT INTO `user_1` VALUES ('62130', 'test1458037923269', '0', '40');
INSERT INTO `user_1` VALUES ('62726', 'test1458037924887', '0', '83');
INSERT INTO `user_1` VALUES ('67568', 'test1458037930827', '0', '41');
INSERT INTO `user_1` VALUES ('67604', 'test1458037930675', '0', '31');
INSERT INTO `user_1` VALUES ('67762', 'test1458037923831', '0', '58');
INSERT INTO `user_1` VALUES ('71974', 'test1458111728822', '0', '85');
INSERT INTO `user_1` VALUES ('74896', 'test1458037869219', '0', '30');
INSERT INTO `user_1` VALUES ('75348', 'test1458111912479', '0', '51');
INSERT INTO `user_1` VALUES ('76058', 'test1458037923864', '0', '74');
INSERT INTO `user_1` VALUES ('76352', 'test1458037924873', '0', '80');
INSERT INTO `user_1` VALUES ('76362', 'test1458037923765', '0', '21');
INSERT INTO `user_1` VALUES ('81874', 'test1458037923634', '0', '30');
INSERT INTO `user_1` VALUES ('82338', 'test1458037923104', '0', '29');
INSERT INTO `user_1` VALUES ('83204', 'test1458037929575', '0', '68');
INSERT INTO `user_1` VALUES ('90864', 'test1458111856499', '0', '74');
INSERT INTO `user_1` VALUES ('91166', 'test1458037930102', '0', '80');
INSERT INTO `user_1` VALUES ('91268', 'test1458037924788', '0', '4');
INSERT INTO `user_1` VALUES ('92878', 'test1458111852136', '0', '85');
INSERT INTO `user_1` VALUES ('93958', 'test1458037929245', '0', '38');
INSERT INTO `user_1` VALUES ('99762', 'test1458037924755', '0', '100');
